#!/bin/bash

docker run -d --name ngrok --link pogomap wernight/ngrok ngrok http pogomap:5000

docker run --rm --link ngrok appropriate/curl sh -c "curl -s http://ngrok:4040/api/tunnels | grep -o 'https\?:\/\/[a-zA-Z0-9\.]\+'"

echo "docker run --rm --link ngrok appropriate/curl sh -c \"curl -s http://ngrok:4040/api/tunnels | grep -o 'https\?:\/\/[a-zA-Z0-9\.]\+'\""
