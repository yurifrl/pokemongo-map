#!/bin/bash

# https://github.com/PokemonGoMap/PokemonGo-Map/wiki/Command-Line-Arguments

docker stop yurifl/pogomap
docker stop ngrok
docker rm yurifl/pogomap
docker rm ngrok

docker build -t yurifl/pogomap .

docker run -d --name pogomap -p 5000:5000 yurifl/pogomap python runserver.py \
  -H 0.0.0.0  \
  -l '-23.517371, -46.201216' \
  -a ptc \
  -u yurifl \
  -p pokepassword \
  -k 'AIzaSyCAyTYi1yeq7-_QMNsXAi_9dL4uFRfkzl8' \
  -st 5 \
  -sd 3

docker run -d --name ngrok --link pogomap wernight/ngrok ngrok http pogomap:5000

docker run --rm --link ngrok appropriate/curl sh -c "curl -s http://ngrok:4040/api/tunnels | grep -o 'https\?:\/\/[a-zA-Z0-9\.]\+'"

echo "docker run --rm --link ngrok appropriate/curl sh -c \"curl -s http://ngrok:4040/api/tunnels | grep -o 'https\?:\/\/[a-zA-Z0-9\.]\+'\""
